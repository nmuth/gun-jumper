extends Sprite

export var direction = Vector2.ZERO
export var SPEED = 300

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func start(_pos, _dir):
	position = _pos
	direction = _dir

func _physics_process(delta):
	position += direction * SPEED * delta

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
