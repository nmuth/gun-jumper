extends Node

onready var tick1 = $Tick1
onready var tick2 = $Tick2
onready var tick3 = $Tick3

func set_ammo(ammo):
	if ammo >= 1:
		tick1.show()
	else:
		tick1.hide()

	if ammo >= 2:
		tick2.show()
	else:
		tick2.hide()

	if ammo >= 3:
		tick3.show()
	else:
		tick3.hide()
