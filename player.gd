extends KinematicBody2D

export var ACCELERATION = 0.5
export var FRICTION = 0.2
export var MOVE_SPEED = 250
export var GRAVITY = 2000
export var JUMP_GRAVITY = 900
export var JUMP_FORCE = -325
export var SHOOT_FORCE = 450
export var SHOOT_FORCE_ON_GROUND = 200
export var MAX_AMMO = 3
export var RELOAD_TIME = 0.5

const Bullet = preload("res://Bullet.tscn")
onready var spawn_point = get_node("../PlayerSpawn")
onready var ammo_meter = get_node("../CanvasLayer/GUI/AmmoMeter")

var velocity = Vector2()
var ammo = MAX_AMMO

var reload_timer = 0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _physics_process(delta):
	var move_dir = 0

	if Input.is_action_pressed("move_left"):
		move_dir -= 1
	if Input.is_action_pressed("move_right"):
		move_dir += 1

	if move_dir == 0:
		$AnimatedSprite.play("idle")
		velocity.x = lerp(velocity.x, 0, FRICTION)
	else:
		$AnimatedSprite.scale.x = move_dir
		$AnimatedSprite.play("walk")
		velocity.x = lerp(velocity.x, move_dir * MOVE_SPEED, ACCELERATION)

	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y += JUMP_FORCE
	
	var grav = GRAVITY
	if Input.is_action_pressed("jump") and velocity.y < 0:
		grav = JUMP_GRAVITY
	velocity.y += grav * delta
	
	if Input.is_action_just_pressed("shoot") and ammo > 0:
		velocity = shoot()

	velocity = move_and_slide(velocity, Vector2.UP)

# Called when the node enters the scene tree for the first time.
func _ready():
	respawn()

func _process(delta):
	if Input.is_action_just_pressed("restart"):
		respawn()

	if is_on_floor() and ammo < MAX_AMMO:
		reload_timer += delta
	else:
		reload_timer = 0

	if reload_timer >= RELOAD_TIME and ammo < MAX_AMMO:
		reload_timer = 0
		set_ammo(ammo + 1)

# shoot a bullet towards the mouse. returns new velocity to be assigned to the player.
func shoot():
	var target_direction  = (get_global_mouse_position() - position).normalized() * -1

	var shoot_force = SHOOT_FORCE
	if is_on_floor():
		shoot_force = SHOOT_FORCE_ON_GROUND
		if target_direction.y <= 0.2:
			# shooting on the ground should give you a little bump unless you shoot up
			target_direction.y = -0.75

	var bullet = Bullet.instance()
	bullet.start($Gun/BulletSpawn.global_position, target_direction * -1)
	get_parent().add_child(bullet)
	set_ammo(ammo - 1)
	
	return target_direction * shoot_force

func set_ammo(v):
	ammo = v
	ammo_meter.set_ammo(ammo)

func respawn():
	position = spawn_point.position
	velocity = Vector2.ZERO
	set_ammo(MAX_AMMO)
